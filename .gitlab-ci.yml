image: ubuntu:20.04

variables:
  # Branch of https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools
  ADAPTATION_TOOLS_BRANCH: halium-10
  # Set to 1 to also produce devel-flashable image with Focal rootfs
  BUILD_DEVEL_FLASHABLE_FOCAL: 1

build-star2lte:
  stage: build
  script:
    - apt update
    - >-
      apt install -y --no-install-recommends
      android-tools-mkbootimg bc bison build-essential ca-certificates cpio curl
      fakeroot flex git kmod libssl-dev libtinfo5 python-is-python2 sudo unzip wget xz-utils
    - wget https://raw.githubusercontent.com/LineageOS/android_system_tools_mkbootimg/lineage-19.1/mkbootimg.py -O /usr/bin/mkbootimg
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-star2lte deviceinfo
    - ./build/build.sh
  artifacts:
    paths:
      - out/*

build-starlte:
  stage: build
  script:
    - apt update
    - >-
      apt install -y --no-install-recommends
      android-tools-mkbootimg bc bison build-essential ca-certificates cpio curl
      fakeroot flex git kmod libssl-dev libtinfo5 python-is-python2 sudo unzip wget xz-utils
    - wget https://raw.githubusercontent.com/LineageOS/android_system_tools_mkbootimg/lineage-19.1/mkbootimg.py -O /usr/bin/mkbootimg
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-starlte deviceinfo
    - ./build/build.sh
  artifacts:
    paths:
      - out/*

build-crownlte:
  stage: build
  script:
    - apt update
    - >-
      apt install -y --no-install-recommends
      android-tools-mkbootimg bc bison build-essential ca-certificates cpio curl
      fakeroot flex git kmod libssl-dev libtinfo5 python-is-python2 sudo unzip wget xz-utils
    - wget https://raw.githubusercontent.com/LineageOS/android_system_tools_mkbootimg/lineage-19.1/mkbootimg.py -O /usr/bin/mkbootimg
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-crownlte deviceinfo
    - ./build/build.sh
  artifacts:
    paths:
      - out/*

flashable-star2lte:
  stage: deploy
  script:
    - apt update
    - apt install -y git img2simg jq sudo wget xz-utils
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-star2lte deviceinfo
    - DEVICE="$(source deviceinfo && echo $deviceinfo_codename)"
    - ./build/fetch-and-prepare-latest-ota.sh "16.04/arm64/android9/devel" "$DEVICE" ota
    - mkdir -p out
    - ./build/system-image-from-ota.sh ota/ubuntu_command out
  artifacts:
    paths:
      - out/boot.img
      - out/system.img
  when: manual

flashable-starlte:
  stage: deploy
  script:
    - apt update
    - apt install -y git img2simg jq sudo wget xz-utils
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-starlte deviceinfo
    - DEVICE="$(source deviceinfo && echo $deviceinfo_codename)"
    - ./build/fetch-and-prepare-latest-ota.sh "16.04/arm64/android9/devel" "$DEVICE" ota
    - mkdir -p out
    - ./build/system-image-from-ota.sh ota/ubuntu_command out
  artifacts:
    paths:
      - out/boot.img
      - out/system.img
  when: manual

flashable-crownlte:
  stage: deploy
  script:
    - apt update
    - apt install -y git img2simg jq sudo wget xz-utils
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-crownlte deviceinfo
    - DEVICE="$(source deviceinfo && echo $deviceinfo_codename)"
    - ./build/fetch-and-prepare-latest-ota.sh "16.04/arm64/android9/devel" "$DEVICE" ota
    - mkdir -p out
    - ./build/system-image-from-ota.sh ota/ubuntu_command out
  artifacts:
    paths:
      - out/boot.img
      - out/system.img
  when: manual

devel-flashable-star2lte:
  stage: deploy
  needs: [ "build-star2lte" ]
  script:
    - apt update
    - apt install -y git img2simg jq sudo wget xz-utils
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-star2lte deviceinfo
    - DEVICE="$(source deviceinfo && echo $deviceinfo_codename)"
    - ./build/prepare-fake-ota.sh out/device_${DEVICE}${DEV_TARBALL_VARIANT}.tar.xz ota
    - mkdir -p out
    - ./build/system-image-from-ota.sh ota/ubuntu_command out
    - mv out/rootfs.img out/ubuntu.img
  artifacts:
    paths:
      - out/boot.img
      - out/ubuntu.img

devel-flashable-starlte:
  stage: deploy
  needs: [ "build-starlte" ]
  script:
    - apt update
    - apt install -y git img2simg jq sudo wget xz-utils
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-starlte deviceinfo
    - DEVICE="$(source deviceinfo && echo $deviceinfo_codename)"
    - ./build/prepare-fake-ota.sh out/device_${DEVICE}${DEV_TARBALL_VARIANT}.tar.xz ota
    - mkdir -p out
    - ./build/system-image-from-ota.sh ota/ubuntu_command out
    - mv out/rootfs.img out/ubuntu.img
  artifacts:
    paths:
      - out/boot.img
      - out/ubuntu.img

devel-flashable-crownlte:
  stage: deploy
  needs: [ "build-crownlte" ]
  script:
    - apt update
    - apt install -y git img2simg jq sudo wget xz-utils
    - >-
      git clone -b $ADAPTATION_TOOLS_BRANCH
      https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git
      build
    - ln -s deviceinfo-crownlte deviceinfo
    - DEVICE="$(source deviceinfo && echo $deviceinfo_codename)"
    - ./build/prepare-fake-ota.sh out/device_${DEVICE}${DEV_TARBALL_VARIANT}.tar.xz ota
    - mkdir -p out
    - ./build/system-image-from-ota.sh ota/ubuntu_command out
    - mv out/rootfs.img out/ubuntu.img
  artifacts:
    paths:
      - out/boot.img
      - out/ubuntu.img


devel-flashable-star2lte-focal:
  extends: devel-flashable-star2lte
  variables:
    ROOTFS_URL: "https://ci.ubports.com/job/focal-hybris-rootfs-arm64/job/master/lastSuccessfulBuild/artifact/ubuntu-touch-android9plus-rootfs-arm64.tar.gz"
    OTA_CHANNEL: "20.04/arm64/android9plus/devel"
    DEV_TARBALL_VARIANT: _usrmerge
  rules:
    # Explicitly re-specify the "default" condition. See:
    # - https://gitlab.com/gitlab-org/gitlab/-/issues/225422
    # - https://docs.gitlab.com/ee/ci/jobs/job_control.html#avoid-duplicate-pipelines
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $BUILD_DEVEL_FLASHABLE_FOCAL == "1"
      when: on_success
    - when: manual
      allow_failure: true

devel-flashable-starlte-focal:
  extends: devel-flashable-starlte
  variables:
    ROOTFS_URL: "https://ci.ubports.com/job/focal-hybris-rootfs-arm64/job/master/lastSuccessfulBuild/artifact/ubuntu-touch-android9plus-rootfs-arm64.tar.gz"
    OTA_CHANNEL: "20.04/arm64/android9plus/devel"
    DEV_TARBALL_VARIANT: _usrmerge
  rules:
    # Explicitly re-specify the "default" condition. See:
    # - https://gitlab.com/gitlab-org/gitlab/-/issues/225422
    # - https://docs.gitlab.com/ee/ci/jobs/job_control.html#avoid-duplicate-pipelines
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $BUILD_DEVEL_FLASHABLE_FOCAL == "1"
      when: on_success
    - when: manual
      allow_failure: true

devel-flashable-crownlte-focal:
  extends: devel-flashable-crownlte
  variables:
    ROOTFS_URL: "https://ci.ubports.com/job/focal-hybris-rootfs-arm64/job/master/lastSuccessfulBuild/artifact/ubuntu-touch-android9plus-rootfs-arm64.tar.gz"
    OTA_CHANNEL: "20.04/arm64/android9plus/devel"
    DEV_TARBALL_VARIANT: _usrmerge
  rules:
    # Explicitly re-specify the "default" condition. See:
    # - https://gitlab.com/gitlab-org/gitlab/-/issues/225422
    # - https://docs.gitlab.com/ee/ci/jobs/job_control.html#avoid-duplicate-pipelines
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $BUILD_DEVEL_FLASHABLE_FOCAL == "1"
      when: on_success
    - when: manual
      allow_failure: true